﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

/*
 * Jonathan Couture 
 */

namespace VSpend.USSpendControllers
{
    public interface IAPIResponse<T> where T : class
    {
        List<T> Results { get; set; }
        PageMetadata pageMetadata { get; set; }
    }

    public class USSpendAPIResponse<T> : IAPIResponse<T> where T : class
    {
        //remove result section
        [JsonProperty("results")]
        public List<T> Results { get; set; }

        //remove page_metadata section
        [JsonProperty("page_metadata")]
        public PageMetadata pageMetadata { get; set; }
    }

    //make the mulitiple page api get to properly work
    public class PageMetadata
    {
        public int count { get; set; }
        public int page { get; set; }

        public bool has_next_page { get; set; }

        public bool has_previous_page { get; set; }

        public string next { get; set; }

        public string current { get; set; }

        public string previous { get; set; }
    }
}
