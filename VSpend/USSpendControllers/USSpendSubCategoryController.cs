﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using VSpend.Entities;
using VSpend.Models;
using VSpend.Data;

/*
 * Jonathan Couture 
 */

namespace VSpend.USSpendControllers
{
    public class USSpendSubCategoryController : MasterUSSpend
    {
        private ApplicationContext context;
        public USSpendSubCategoryController(ApplicationContext ac)
        {
            context = ac;
        }

        public override bool GetData()
        {
            StartProcess = DateTime.Now;

            //If we don't know the fiscal year, assume this year.
            FiscalYear = FiscalYear == 0 ? DateTime.Now.Year : FiscalYear;

            //Instantiate the Log class
            LogCrud log = new LogCrud(context);


            //Instantiate a FinanceCrud object so we can write new Finance data
            FinanceCrud finCrud = new FinanceCrud(context);

            //Instantiate a SubcategoryCrud object so we can write new Subcategor data
            SubcategoryCrud subCrud = new SubcategoryCrud(context);


            //URL to retrieve Finance Information from USSPend.gov
            string baseURL = "financial_spending/object_class/?fiscal_year={0}&funding_agency_id={1}&major_object_class_code={2}";

            //Get list of existing Finance Entries for this Fiscal Year.
            List<Finance> finances = finCrud.Get().FindAll(a => a.FiscalYear == FiscalYear);

            using (var httpClient = new HttpClient())
            {

                try
                {
                    log.Create(new Log { Description = String.Format("STARTING - Financial Subcategory Spending Data Ingestion from US Spending for Fiscal Year {0}", FiscalYear.ToString()), IsError = false });

                    //Loop through each agency
                    foreach (Finance finance in finances)
                    {

                        //Retrieve Finance Data for this Agency and this Fiscal Year
                        log.Create(new Log { Description = String.Format("     Retrieving SubCategory data for Agency ID {0} - Category: {1} - Fiscal Year {2}",finance.AgencyID.ToString(), finance.CategoryID, FiscalYear.ToString()), IsError = false });

                        //Call USSPend.gov API and retrieve all Spending by Category for this Agency, this fiscal year
                        string url = USSPendURL + String.Format(baseURL, FiscalYear.ToString(), finance.AgencyID,finance.CategoryID);
                        var ReturnValue = httpClient.GetStringAsync(new Uri(url)).Result;

                        //Read the JSON that was received from the API and add to a collection of Subcategories
                        var APIResult = JsonConvert.DeserializeObject<USSpendAPIResponse<Subcategory>>(ReturnValue.ToString());

                        List<Subcategory> subCategories = APIResult.Results;

                        //Loop through each finance entry for this Agency/year.
                        foreach (Subcategory subCategory in subCategories)
                        {
                            //Complete Properties for SubCategory object with AgencyID, CategoryID and Fiscal Year
                            subCategory.AgencyID = finance.AgencyID;
                            subCategory.CategoryID = finance.CategoryID;
                            subCategory.FiscalYear = FiscalYear;

                            //Add Subcategory Data
                            subCrud.Create(subCategory);

                        }

                        //Log the sucessful API Retrieval.
                        log.Create(new Log { Description = String.Format("     {3} SubCategory Entries retrieved from USSpend for Agency ID:{0} - Category: {1} - Fiscal Year: {2}", finance.AgencyID.ToString(), finance.CategoryID, FiscalYear.ToString(), subCategories.Count.ToString("n0")), IsError = false });


                    }


                }
                catch (Exception ex) //Failed API Call
                {
                    //Log the error
                    log.Create(new Log { Description = "ERROR retrieving Subcategory Data from USSpend -> " + ex.Message, IsError = true });

                    //We're done
                    return false;
                }

            }

            EndProcess = DateTime.Now;
            log.Create(new Log { Description = String.Format("COMPLETED - SubCategory Spending Data Ingestion from US Spending for Fiscal Year {0} - Elapsed Time: {1}", FiscalYear.ToString(), GetElapsedTime()), IsError = false });

            return true;
        }
    }
}
