﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VSpend.Entities;

/*
 * Jonathan Couture 
 */

namespace VSpend.USSpendControllers
{
    public class MasterUSSpend
    {
        protected string USSPendURL = "https://api.usaspending.gov/api/v2/";
        protected DateTime StartProcess;
        protected DateTime EndProcess;


        public int FiscalYear { get; set; }

        public virtual bool GetData()
        {
            return true;
        }

        //log data
        protected string GetElapsedTime()
        {
            TimeSpan elapsed = EndProcess.Subtract(StartProcess);

            string ElapsedTime = "";


            if (elapsed.Days > 0)
                ElapsedTime += String.Format("{0} Days ", elapsed.Days.ToString());

            if (elapsed.Hours > 0)
                ElapsedTime += String.Format("{0} Hours ", elapsed.Hours.ToString());

            if (elapsed.Minutes > 0)
                ElapsedTime += String.Format("{0} Minutes ", elapsed.Minutes.ToString());

            if (elapsed.Seconds > 0)
                ElapsedTime += String.Format("{0} Seconds ", elapsed.Seconds.ToString());

            if (elapsed.Milliseconds > 0)
                ElapsedTime += String.Format("{0} Milliseconds ", elapsed.Milliseconds.ToString());

            return ElapsedTime;

        }
    }
}
