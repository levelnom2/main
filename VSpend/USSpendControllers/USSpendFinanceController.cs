﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using VSpend.Entities;
using VSpend.Models;
using VSpend.Data;

/*
 * Jonathan Couture 
 */

namespace VSpend.USSpendControllers
{
    public class USSpendFinanceController : MasterUSSpend
    {
        private ApplicationContext context;
        public USSpendFinanceController(ApplicationContext ac)
        {
            context = ac;
        }

        public override bool GetData()
        {
            StartProcess = DateTime.Now;

            //If we don't know the fiscal year, assume this year.
            FiscalYear = FiscalYear == 0 ? DateTime.Now.Year : FiscalYear;

            //Instantiate the Log class
            LogCrud log = new LogCrud(context);

            //Instantiate a CategoryCrud object so we can read/write to the table.
            CategoryCrud catCrud = new CategoryCrud(context);

            //Instantiate a FinanceCrud object so we can write new Finance data
            FinanceCrud finCrud = new FinanceCrud( context);

            //Get list of current Categories in tables
            List<Category> ExistingCategories = catCrud.Get();

            //URL to retrieve Finance Information from USSPend.gov
            string baseURL = "financial_spending/major_object_class/?fiscal_year={0}&funding_agency_id={1}";

            //Get list of existing Agencies.
            List<Agency> agencies = new AgencyCrud(context).Get();

            using (var httpClient = new HttpClient())
            {

                try
                {
                    log.Create(new Log { Description =String.Format( "STARTING - Financial Spending Data Ingestion from US Spending for Fiscal Year {0}",FiscalYear.ToString()), IsError = false });

                    //Loop through each agency
                    foreach (Agency agency in agencies)
                    {

                        //Retrieve Finance Data for this Agency and this Fiscal Year
                        log.Create(new Log { Description = String.Format("     Retrieving Finance data for Agency ID {0} - {1} - Fiscal Year {2}", agency.ID.ToString(), agency.Name, FiscalYear.ToString()), IsError = false });

                        //Call USSPend.gov API and retrieve all Spending by Category for this Agency, this fiscal year
                        string url = USSPendURL + String.Format(baseURL, FiscalYear.ToString(), agency.ID);
                        var ReturnValue = httpClient.GetStringAsync(new Uri(url)).Result;

                        //Read the JSON that was received from the API and add to a collection of Agencies
                        var APIResult = JsonConvert.DeserializeObject<USSpendAPIResponse<Finance>>(ReturnValue.ToString());

                        List<Finance> finances = APIResult.Results;

                        //Loop through each finance entry for this Agency/year.
                        foreach (Finance finance in finances)
                        {
                            //Do we have this category in the Category Table?
                            if (ExistingCategories.Find(a => a.CategoryID == finance.CategoryID) == null)
                            {
                                log.Create(new Log { Description = String.Format("Adding Category {0} - {1}", finance.CategoryID, finance.Name), IsError = false });
                                Category newCategory = new Category { CategoryID = finance.CategoryID, Name = finance.Name };
                                new CategoryCrud(context).Create(newCategory);

                                ExistingCategories.Add(newCategory);

                            }

                            //Complete Properties for Finance object with AgencyID and Fiscal Year
                            finance.AgencyID = agency.ID;
                            finance.FiscalYear = FiscalYear;

                            //Add Financial Data
                            finCrud.Create(finance);

                        }

                        //Log the sucessful API Retrieval.
                        log.Create(new Log { Description = String.Format("     {3} Financial Entries retrieved from USSpend for Agency ID:{0} - {1} - Fiscal Year: {2}", agency.ID.ToString(),agency.Name,FiscalYear.ToString(), finances.Count.ToString("n0")), IsError = false });

                       
                    }


                }
                catch (Exception ex) //Failed API Call
                {
                    //Log the error
                    log.Create(new Log { Description = "ERROR retrieving Finance Data from USSpend -> " + ex.Message, IsError = true });

                    //We're done
                    return false;
                }

            }

            EndProcess = DateTime.Now;
            log.Create(new Log { Description = String.Format("COMPLETED - Financial Spending Data Ingestion from US Spending for Fiscal Year {0} - Elapsed Time: {1}", FiscalYear.ToString(),GetElapsedTime()), IsError = false });

            return true;
        }
    }
}

