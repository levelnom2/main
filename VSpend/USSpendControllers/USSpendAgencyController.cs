﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using VSpend.Entities;
using VSpend.Models;
using VSpend.Data;

/*
 * Jonathan Couture 
 */

namespace VSpend.USSpendControllers
{
    public class USSpendAgencyController : MasterUSSpend
    {
        private ApplicationContext context;
        public USSpendAgencyController(ApplicationContext ac)
        {
            context = ac;
        }

        public override bool GetData()
        {
            //Instantiate the Log class
            LogCrud log = new LogCrud(context);


            //Call USSPending.gov API and retrieve all the agencies
            using (var httpClient = new HttpClient())
            {
                List<Agency> agencies;
                try
                {
                    log.Create(new Log { Description = "STARTING - Retriving Agency data from USSpend", IsError = false });

                    //Call USSPend.gov API and retrieve all agencies
                    string url = USSPendURL + "references/toptier_agencies/";
                    var ReturnValue = httpClient.GetStringAsync(new Uri(url)).Result;

                    //Read the JSON that was received from the API and add to a collection of Agencies
                    var APIResult = JsonConvert.DeserializeObject<USSpendAPIResponse<Agency>>(ReturnValue.ToString());

                    agencies = APIResult.Results; 

                    //Log the sucessful API Retrieval.
                    log.Create(new Log { Description =agencies.Count.ToString("n0") + " agencies retrieve from USSpend", IsError = false });

                }
                catch (Exception ex) //Failed API Call
                {
                    //Log the error
                    log.Create(new Log { Description =  "ERROR retrieving Agencies from USSpend -> " + ex.Message, IsError = true });

                    //We're done
                    return false;
                }

                try
                {

                    //Get the Current list of Agencies in the table.
                    //If we have it already, we'll just update. Otherwise we'll insert
                    List<Agency> currentList = new AgencyCrud(context).Get();

                    //Get an AgencyCrud object to be able to insert/update
                    AgencyCrud crud = new AgencyCrud(context);

                    //Loop through all the agencies retrieved from the API
                    foreach (var agency in agencies)
                    {
                        //Do we already have this Agency in our table?
                        var currentItem = context.Agency.Find(agency.ID);

                        //No? Then Create
                        if (currentItem == null)
                        {
                            log.Create(new Log { Description = String.Format("Creating agency {0} - {1}",agency.ID.ToString(),agency.Name), IsError = false });
                            crud.Create(agency);

                        }
                        else  //Already exists. Just update
                        {
                            log.Create(new Log { Description = String.Format("Updating agency {0} - {1}", agency.ID.ToString(), agency.Name), IsError = false });

                            crud.Update(agency);
                        }

                    }
                }
                catch (Exception ex)  //Failed DB transaction
                {
                    log.Create(new Log { Description = "ERROR saving Agency from USSpend -> " + ex.Message, IsError = true });

                    return false;
                }

                //If we're here. We're done with Agency data Ingestion.
                log.Create(new Log { Description = "COMPLETED - Retriving Agency data from USSpend", IsError = false });

                return true;
            }
        }
    }
}
