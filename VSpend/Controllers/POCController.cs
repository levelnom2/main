﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using VSpend.Data;
using VSpend.Entities;
using VSpend.Models;
using Newtonsoft.Json;
using VSpend.USSpendControllers;

/*
 * Jonathan Couture 
 */

namespace VSpend.Controllers
{
    [Route("API/POC")]
    [ApiController]
    public class POCController : ControllerBase
    {
        private ApplicationContext context;

        public POCController(ApplicationContext ac)
        {
            context = ac;
        }

        [HttpGet]
        public IActionResult GetPOCList()
        {
            //default year
            int FiscalYear = 2019;

            //update agency
            USSpendAgencyController ac = new USSpendAgencyController(context);
            ac.GetData();

            //update finance
            USSpendFinanceController fc = new USSpendFinanceController(context);
            fc.FiscalYear = FiscalYear;
            fc.GetData();

            //update subCategory
            USSpendSubCategoryController sc = new USSpendSubCategoryController(context);
            sc.FiscalYear = FiscalYear;
            sc.GetData();


            return Ok();
        }        
    }

    public interface IDataResponse<T> where T : class
    {
        List<T> Results { get; set; }
    }

    //remove result section
    public class DataResponse<T> : IDataResponse<T> where T : class
    {
        [JsonProperty("results")]
        public List<T> Results { get; set; }
    }

    public class SpendAgency
    {
        public string agency_id { get; set; }
        public string abbreviation { get; set; }
        public string agency_name { get; set; }
        public string congressional_justification_url { get; set; }
        public string active_fy { get; set; }
        public string active_fq { get; set; }
        public string outlay_amount { get; set; }
        public string obligated_amount { get; set; }
        public string budget_authority_amount { get; set; }
        public string current_total_budget_authority_amount { get; set; }
        public string percentage_of_total_budget_authority_amount { get; set; }
    }
}