﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VSpend.Data;
using VSpend.Entities;
using VSpend.Models;

/*
 * Jonathan Couture 
 */

namespace VSpend.Controllers
{
    [Route("api/update")]
    [ApiController]
    public class MasterController : ControllerBase
    {
        private ApplicationContext context;
        public MasterController(ApplicationContext ac)
        {
            context = ac;

        }
        [HttpGet]
        public IActionResult updater()
        {
            Dictionary<String, String> _POCList = new Dictionary<String, String>();
            //remove result section
            _POCList.Add("Agency", "result");
            Dictionary<String, String> Agency = AgencyUpdate();
            foreach (var item in Agency)
            {
                _POCList.Add("Agency " + item.Key, item.Value);
            }

            _POCList.Add("", "");
            _POCList.Add("Award", "result");
            Dictionary<String, String> Award = AwardUpdate();
            foreach (var item in Award)
            {
                _POCList.Add("Award " + item.Key, item.Value);
            }

            _POCList.Add(".", "");
            _POCList.Add("Finance", "result");
            Dictionary<String, String> Finance = FinanceUpdate();
            foreach (var item in Finance)
            {
                _POCList.Add("Finance " + item.Key, item.Value);
            }

            _POCList.Add("..", "");
            _POCList.Add("Category", "result");
            //Dictionary<String, String> Category = CategoryUpdate();
            //foreach (var item in Category)
            //{
            //    _POCList.Add("Category " + item.Key, item.Value);
            //}

            _POCList.Add("...", "");
            _POCList.Add("Subcategory", "result");
            Dictionary<String, String> Subcategory = SubcategoryUpdate();
            foreach (var item in Subcategory)
            {
                _POCList.Add("Subcategory " + item.Key, item.Value);
            }

            _POCList.Add("|", "");
            _POCList.Add("Log", "result");
            Dictionary<String, String> Log = LogUpdate();
            foreach (var item in Log)
            {
                _POCList.Add("Log " + item.Key, item.Value);
            }

            return Ok(_POCList);
        }
        private Dictionary<String, String> AgencyUpdate()
        {
            return AgencyUpdate(DateTime.Now.Year);
        }

        private Dictionary<String, String> AgencyUpdate(int year)
        {
            Dictionary<String, String> _POCList = new Dictionary<String, String>();
            AgencyCrud test = new AgencyCrud( context);
            Agency fin = new Agency();
            fin.Name = "name";

            bool create = test.Create(fin);
            _POCList.Add("create", create.ToString());

            var getAll = test.Get();
            int count = 0;
            foreach (var item in getAll)
            {
                count++;
                _POCList.Add("getAll " + count, item.Name);
            }

            fin.Name = "name";
            bool update = test.Update(fin);
            _POCList.Add("update", update.ToString());

            Agency get = test.Get(1);
            _POCList.Add("get", get.Name);

            bool delete = test.Delete(1);
            _POCList.Add("delete", delete.ToString());

            return _POCList;
        }
        private Dictionary<String, String> AwardUpdate()
        {
            return AwardUpdate(DateTime.Now.Year);
        }

        private Dictionary<String, String> AwardUpdate(int year)
        {
            Dictionary<String, String> _POCList = new Dictionary<String, String>();
            AwardCrud test = new AwardCrud(1000, context);
            Award fin = new Award();
            fin.AgencyID = 1;
            fin.Amount = 9;
            fin.Vendor = "bert";

            bool create = test.Create(fin);
            _POCList.Add("create", create.ToString());

            var getAll = test.Get();
            int count = 0;
            foreach (var item in getAll)
            {
                count++;
                _POCList.Add("getAll " + count, item.Vendor);
            }

            fin.Vendor = "terb";
            bool update = test.Update(fin);
            _POCList.Add("update", update.ToString());

            Award get = test.Get(1);
            _POCList.Add("get", get.Vendor);

            bool delete = test.Delete(1);
            _POCList.Add("delete", delete.ToString());

            return _POCList;
        }
        private Dictionary<String, String> FinanceUpdate()
        {
            return FinanceUpdate(DateTime.Now.Year);
        }

        private Dictionary<String, String> FinanceUpdate(int year)
        {
            Dictionary<String, String> _POCList = new Dictionary<String, String>();
            FinanceCrud test = new FinanceCrud( context);
            Finance fin = new Finance();
            fin.AgencyID = 1;
           // fin.AuthorityAmount = 9;
            fin.ObligatedAmount = 8;
            //fin.OutlayAmount = 7;

            bool create = test.Create(fin);
            _POCList.Add("create", create.ToString());

            var getAll = test.Get();
            int count = 0;
            foreach (var item in getAll)
            {
                count++;
                _POCList.Add("getAll " + count, item.ID.ToString());
            }

            fin.ObligatedAmount = 6;
            bool update = test.Update(fin);
            _POCList.Add("update", update.ToString());

            Finance get = test.Get(1);
            _POCList.Add("get", get.ID.ToString());

            bool delete = test.Delete(1);
            _POCList.Add("delete", delete.ToString());

            return _POCList;
        }
        //private Dictionary<String, String> CategoryUpdate()
        //{
        //    return CategoryUpdate(DateTime.Now.Year);
        //}

        //private Dictionary<String, String> CategoryUpdate(int year)
        //{
        //    Dictionary<String, String> _POCList = new Dictionary<String, String>();
        //    CategoryCrud test = new CategoryCrud(1000, context);
        //    Category fin = new Category();
        //    fin.AgencyID = 1;
        //    fin.Amount = 9;
        //    fin.CategoryClass = 8;
        //    fin.Name = "cat";

        //    bool create = test.Create(fin);
        //    _POCList.Add("create", create.ToString());

        //    var getAll = test.Get();
        //    int count = 0;
        //    foreach (var item in getAll)
        //    {
        //        count++;
        //        _POCList.Add("getAll " + count, item.Name);
        //    }

        //    fin.Name = "Kot";
        //    bool update = test.Update(fin);
        //    _POCList.Add("update", update.ToString());

        //    Category get = test.Get(1);
        //    _POCList.Add("get", get.Name);

        //    bool delete = test.Delete(1);
        //    _POCList.Add("delete", delete.ToString());

        //    return _POCList;
        //}
        private Dictionary<String, String> SubcategoryUpdate()
        {
            return SubcategoryUpdate(DateTime.Now.Year);
        }

        private Dictionary<String, String> SubcategoryUpdate(int year)
        {
            Dictionary<String, String> _POCList = new Dictionary<String, String>();
            SubcategoryCrud test = new SubcategoryCrud( context);
            Subcategory fin = new Subcategory();
            fin.Name = "huy";
            fin.CategoryID = "9";

            bool create = test.Create(fin);
            _POCList.Add("create", create.ToString());

            var getAll = test.Get();
            int count = 0;
            foreach (var item in getAll)
            {
                count++;
                _POCList.Add("getAll " + count, item.Name);
            }

            fin.Name = "yuh";
            bool update = test.Update(fin);
            _POCList.Add("update", update.ToString());

            Subcategory get = test.Get(1);
            _POCList.Add("get", get.Name);

            bool delete = test.Delete(1);
            _POCList.Add("delete", delete.ToString());

            return _POCList;
        }
        private Dictionary<String, String> LogUpdate()
        {
            return LogUpdate(DateTime.Now.Year);
        }

        private Dictionary<String, String> LogUpdate(int year)
        {
            Dictionary<String, String> _POCList = new Dictionary<String, String>();
            LogCrud test = new LogCrud(context);
            Log fin = new Log();
            fin.IsError = false;
            fin.Description = "09";

            bool create = test.Create(fin);
            _POCList.Add("create", create.ToString());

            var getAll = test.Get();
            int count = 0;
            foreach (var item in getAll)
            {
                count++;
                _POCList.Add("getAll " + count, item.Description);
            }

            fin.Description = "9999999999999999999999999";
            bool update = test.Update(fin);
            _POCList.Add("update", update.ToString());

            Log get = test.Get(1);
            _POCList.Add("get", get.Description);

            bool delete = test.Delete(1);
            _POCList.Add("delete", delete.ToString());

            return _POCList;
        }
    }
}