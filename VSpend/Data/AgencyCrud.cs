﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VSpend.Entities;
using VSpend.Models;

/*
 * Jonathan Couture 
 */

namespace VSpend.Data
{
    public class AgencyCrud
    {
        private ApplicationContext context;

        public AgencyCrud(ApplicationContext ac)
        {
            context = ac;

        }

        public bool Create(Agency obj)
        {
            //prevent creation of null objects
            if (obj == null)
            {
                return false;
            }

            //if empty, fill in minimal default data
            if (obj.Name != "")
            {
                context.Agency.Add(obj);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            //attempt to fetch this object that is being deleted
            var existingItem = context.Agency.FirstOrDefault(q => q.ID == id);
            //if it existed
            if (existingItem != null)
            {
                //remove it
                context.Agency.Remove(existingItem);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }


        public List<Agency> Get()
        {
            return context.Agency.ToList();
        }

        public Agency Get(int id)
        {
            //attempt to get the given object
            var currentItem = context.Agency.Where(s => s.ID == id).ToList();
            //if it is not found or muliple object found...
            if (currentItem.Count != 1)
            {
                //make a new empty object
                return new Agency();
            }
            else
            {
                return currentItem[0];
            }
        }

        public bool Update(Agency obj)
        {
            //do not run if the object does not exist
            if (obj == null)
                return false;

            //check if it is a valid object
            if (obj.Name != "" && obj.ID >= 0)
            {
                //what is the current entry in the DB
                var currentItem = context.Agency.Find(obj.ID);

                //Update with the new data received
                context.Entry(currentItem).CurrentValues.SetValues(obj);

                //Save Changes
                context.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
