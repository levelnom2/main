﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VSpend.Entities;
using VSpend.Models;

/*
 * Jonathan Couture 
 */

namespace VSpend.Data
{
    public class CategoryCrud
    {
        private ApplicationContext context;

        public CategoryCrud(ApplicationContext ac)
        {
            context = ac;
        }

        public bool Create(Category obj)
        {
            //prevent creation of null objects
            if (obj == null)
                return false;


            context.Category.Add(obj);
            context.SaveChanges();
            return true;

        }

        public bool Delete(int id)
        {
            //attempt to fetch this object that is being deleted
            var existingItem = context.Category.FirstOrDefault(q => q.ID == id);
            //if it existed
            if (existingItem != null)
            {
                //remove it
                context.Category.Remove(existingItem);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }


        public List<Category> Get()
        {
            return context.Category.ToList();
        }

        public Category Get(int id)
        {
            //attempt to get the given object
            var currentItem = context.Category.Where(s => s.ID == id).ToList();
            //if it is not found or muliple object found...
            if (currentItem.Count != 1)
            {
                //make a new empty object
                return new Category();
            }
            else
            {
                return currentItem[0];
            }
        }

        public bool Update(Category obj)
        {
            //do not run if the object does not exist
            if (obj == null)
                return false;


            //what is the current entry in the DB
            var currentItem = context.Category.Find(obj.ID);

            //Update with the new data received
            context.Entry(currentItem).CurrentValues.SetValues(obj);

            //Save Changes
            context.SaveChanges();

            return true;

        }
    }
}
