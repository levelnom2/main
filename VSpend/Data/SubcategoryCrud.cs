﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VSpend.Entities;
using VSpend.Models;

/*
 * Jonathan Couture 
 */

namespace VSpend.Data
{
    public class SubcategoryCrud
    {
        private ApplicationContext context;

        public SubcategoryCrud(ApplicationContext ac)
        {
            context = ac;
        }

        public bool Create(Subcategory obj)
        {
            //prevent creation of null objects
            if (obj == null)
                return false;

            //if empty, fill in minimal default data
            if (obj.Amount > 0 && obj.Name != "" && !String.IsNullOrEmpty( obj.CategoryID))
            {

                context.Subcategory.Add(obj);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            //attempt to fetch this object that is being deleted
            var existingItem = context.Subcategory.FirstOrDefault(q => q.ID == id);
            //if it existed
            if (existingItem != null)
            {
                //remove it
                context.Subcategory.Remove(existingItem);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }


        public List<Subcategory> Get()
        {
            return context.Subcategory.ToList();
        }

        public Subcategory Get(int id)
        {
            //attempt to get the given object
            var currentItem = context.Subcategory.Where(s => s.ID == id).ToList();
            //if it is not found or muliple object found...
            if (currentItem.Count != 1)
            {
                //make a new empty object
                return new Subcategory();
            }
            else
            {
                return currentItem[0];
            }
        }

        public bool Update(Subcategory obj)
        {
            //do not run if the object does not exist
            if (obj == null)
                return false;

            if (obj.Amount > 0 && obj.Name != "" && !String.IsNullOrEmpty(obj.CategoryID) && obj.ID >= 0)
            {
                //what is the current entry in the DB
                var currentItem = context.Subcategory.Find(obj.ID);

                //Update with the new data received
                context.Entry(currentItem).CurrentValues.SetValues(obj);

                //Save Changes
                context.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
