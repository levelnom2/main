﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VSpend.Entities;
using VSpend.Models;

/*
 * Jonathan Couture 
 */

namespace VSpend.Data
{
    public class AwardCrud
    {
        private ApplicationContext context;
        private int year;

        public AwardCrud(ApplicationContext ac)
        {
            FinalConstructor(DateTime.Now.Year, ac);
        }

        public AwardCrud(int Year, ApplicationContext ac)
        {
            FinalConstructor(year, ac);
        }

        private void FinalConstructor(int Year, ApplicationContext ac)
        {
            context = ac;
            year = Year;
        }

        public bool Create(Award obj)
        {
            //prevent creation of null objects
            if (obj == null)
                return false;

            //if empty, fill in minimal default data
            if (obj.Amount > 0 && obj.Vendor != "" && obj.AgencyID >= 0)
            {

                obj.FinancialYear = year;
                context.Award.Add(obj);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            //attempt to fetch this object that is being deleted
            var existingItem = context.Award.FirstOrDefault(q => q.ID == id);
            //if it existed
            if (existingItem != null)
            {
                context.Award.Remove(existingItem);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }


        public List<Award> Get()
        {
            return context.Award.ToList();
        }

        public Award Get(int id)
        {
            //attempt to get the give object
            var currentItem = context.Award.Where(s => s.ID == id).ToList();
            //if it is not found or muliple object found...
            if (currentItem.Count != 1)
            {
                //make a new empty object
                return new Award();
            }
            else
            {
                return currentItem[0];
            }
        }

        public bool Update(Award obj)
        {
            //do not run if the object does not exist
            if (obj == null)
                return false;

            if (obj.Amount > 0 && obj.Vendor != "" && obj.AgencyID >= 0 && obj.ID >= 0)
            {
                //what is the current entry in the DB
                var currentItem = context.Award.Find(obj.ID);

                //Update with the new data received
                context.Entry(currentItem).CurrentValues.SetValues(obj);

                //Save Changes
                context.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
