﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VSpend.Entities;
using VSpend.Models;

/*
 * Jonathan Couture 
 */

namespace VSpend.Data
{
    public class LogCrud
    {
        private ApplicationContext context;

        public LogCrud(ApplicationContext ac)
        {
             context = ac;
          
        }

        public bool Create(Log obj)
        {
            //prevent creation of null objects
            if (obj == null)
                return false;

            //if empty, fill in minimal default data
            if (obj.Description != "")
            {
                obj.Date =  DateTime.Now;

                context.Log.Add(obj);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            //attempt to fetch this object that is being deleted
            var existingItem = context.Log.FirstOrDefault(q => q.ID == id);
            //if it existed
            if (existingItem != null)
            {
                //remove it
                context.Log.Remove(existingItem);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }


        public List<Log> Get()
        {
            return context.Log.ToList();
        }

        public Log Get(int id)
        {
            //attempt to get the given object
            var currentItem = context.Log.Where(s => s.ID == id).ToList();
            //if it is not found or muliple object found...
            if (currentItem.Count != 1)
            {
                //make a new empty object
                return new Log();
            }
            else
            {
                return currentItem[0];
            }
        }

        public bool Update(Log obj)
        {
            //do not run if the object does not exist
            if (obj == null)
                return false;

            if (obj.Description != "" && obj.ID >= 0)
            {
                //what is the current entry in the DB
                var currentItem = context.Log.Find(obj.ID);

                //Update with the new data received
                context.Entry(currentItem).CurrentValues.SetValues(obj);

                //Save Changes
                context.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
