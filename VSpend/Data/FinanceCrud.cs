﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VSpend.Entities;
using VSpend.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;

/*
 * Jonathan Couture 
 */

namespace VSpend.Data
{
    public class FinanceCrud
    {
        private ApplicationContext context;

        public FinanceCrud(ApplicationContext ac)
        {
            context = ac;
        }

        public bool Create(Finance obj)
        {
            //prevent creation of null objects
            if (obj == null)
                return false;

            //if empty, fill in minimal default data
            if (obj.ObligatedAmount > 0 && obj.AgencyID >= 0 && obj.FiscalYear>0)
            {
                //remove it
                context.Finance.Add(obj);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            //attempt to fetch this object that is being deleted
            var existingItem = context.Finance.FirstOrDefault(q => q.ID == id);
            //if it existed
            if (existingItem != null)
            {
                context.Finance.Remove(existingItem);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }


        public List<Finance> Get()
        {
            return context.Finance.ToList();
        }

        public Finance Get(int id)
        {
            //attempt to get the given object
            var currentItem = context.Finance.Where(s => s.ID == id).ToList();
            //if it is not found or muliple object found...
            if (currentItem.Count != 1)
            {
                //make a new empty object
                return new Finance();
            }
            else
            {
                return currentItem[0];
            }
        }

        public bool Update(Finance obj)
        {
            //do not run if the object does not exist
            if (obj == null)
                return false;

            if (obj.ObligatedAmount > 0  && obj.AgencyID >= 0 && obj.ID >= 0 && obj.FiscalYear > 0)
            {
                //what is the current entry in the DB
                var currentItem = context.Finance.Find(obj.ID);

                //Update with the new data received
                context.Entry(currentItem).CurrentValues.SetValues(obj);

                //Save Changes
                context.SaveChanges();
                
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
