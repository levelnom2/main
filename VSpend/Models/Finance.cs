﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

/*
 * Jonathan Couture 
 */

namespace VSpend.Models
{
    public class Finance
    {
 
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int AgencyID { get; set; }

        public int FiscalYear { get; set; }

        [JsonProperty("major_object_class_code")]
        public string CategoryID { get; set; }

        [NotMapped]
        [JsonProperty("major_object_class_name")]
        public string Name { get; set; }

        [JsonProperty("obligated_amount")]
        public double ObligatedAmount { get; set; }
        
    }
}
