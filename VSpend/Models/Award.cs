﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

/*
 * Jonathan Couture 
 */

namespace VSpend.Models
{
    public class Award
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int AgencyID { get; set; }
        public double Amount { get; set; }
        public string Vendor { get; set; }
        public int FinancialYear { get; set; }
    }
}
