﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

/*
 * Jonathan Couture 
 */

namespace VSpend.Models
{
    public class Agency
    {
        [Key]
        [JsonProperty("agency_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [JsonProperty("agency_name")]
        public string Name { get; set; }

        [JsonProperty("abbreviation")]
        public string Abrevation { get; set; }
    }
}
