﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

/*
 * Jonathan Couture 
 */

namespace VSpend.Models
{
    public class Subcategory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string CategoryID { get; set; }

        public int AgencyID { get; set; }

        public int FiscalYear { get; set; }

        [JsonProperty("object_class_code")]
        public string SubCategoryID { get; set; }

        [JsonProperty("object_class_name")]
        public string Name { get; set; }

        [JsonProperty("obligated_amount")]
        public double Amount { get; set; }
    }
}
