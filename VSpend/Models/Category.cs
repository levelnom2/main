﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

/*
 * Jonathan Couture 
 */

namespace VSpend.Models
{
    public class Category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [JsonProperty("major_object_class_code")]
        public string CategoryID { get; set; }

        [JsonProperty("major_object_class_name")]
        public string Name { get; set; }
    }

    public class Category1
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int AgencyID { get; set; }
        public int CategoryClass { get; set; }
        public string Name { get; set; }
        public double Amount { get; set; }

    }
}
